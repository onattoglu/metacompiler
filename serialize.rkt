#lang racket

(define (serialize-str-param idx)
  (list 
   (string-append "OpData[ " idx "] = AllocateString(")
   (string-append "GetStringFromOffset(Module->Strings, Module->OpData[Op->DataOffset + " idx "]), sTemporaryStringsBuffer, &sTemporaryStringsBufferOffset);")))

(define (serialize-int/f32-str-param idx)
  (list 
   (string-append "if (( 1 << " idx ") & Op->ParamTypes) {")
   (serialize-str-param idx)
   "} else {"
   (string-append "OpData[ " idx "] = Module->OpData[Op->DataOffset + " idx "];")
   "}"))

(define (generate-c-serializer op-sym op-type params)
  (list
   (string-append "/* op: " (symbol->string op-sym) " --- type: " (number->string op-type) " */")
   (string-append "u32 Op" (number->string op-type) "_SerializeFunc(module* Module, op_node* Op) {")
   "u32 OpDataOffset = Module->TempOpDataOffset;"
   "s32* OpData = &Module->TempOpData[OpDataOffset];"
   (map (lambda (param param-idx) 
          (let ((param-type (car param))
                (idx (number->string param-idx)))
          (case param-type
            ((int f32 enum bool)
             (serialize-int/f32-str-param idx))
            ((str code)
             (serialize-str-param idx))) )) params (build-list (length params) values))
   (string-append "Module->TempOpDataOffset += " (number->string (length params)) ";")
   "return OpDataOffset;"
   "}"
   ))

(provide generate-c-serializer)